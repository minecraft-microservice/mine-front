'use client'

import DashboardNavigation from '@/components/navigation/DashboardNavigation'

export default function DashboardLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <section>
      <div style={{ margin: '50px', background: '#efefef' }}>
        Layout
        <DashboardNavigation />
        {children}
      </div>
    </section>
  )
}
