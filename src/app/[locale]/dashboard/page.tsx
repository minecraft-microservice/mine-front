// pages/index.js
'use client'

import { useState } from 'react'
import useSocket from '../hooks/useSocket'

import TestLayout from '../../../components/TestLayout'

export default function Index() {

  return (
    <div>
      Dashboard
      <TestLayout />
    </div>
  )
}
