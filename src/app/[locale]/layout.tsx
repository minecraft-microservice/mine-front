// Next
import { NextIntlClientProvider, useLocale } from 'next-intl'
import { getMessages } from 'next-intl/server'
import type { Metadata } from 'next'

// Components
import BasicLayout from '@/components/layout/BasicLayout'
// import MainNavigation from '@/components/navigation/MainNavigation'

export const metadata: Metadata = {
  title: 'Home Page',
  description: 'Home Page Description',
}

/**
 * This Layout Covers all Sub Pages
 */
export default async function LocaleLayout({
  children,
  params: { locale },
}: {
  children: React.ReactNode
  params: { locale: string }
}) {
  // Providing all messages to the client
  // side is the easiest way to get started
  const messages = await getMessages()

  const dir = locale === 'en' ? 'ltr' : 'rtl'

  return (
    <html dir={dir} lang={locale} data-theme="bumblebee">
      <body>
        <NextIntlClientProvider messages={messages}>
          <div className={''}>
            {children}
          </div>
        </NextIntlClientProvider>
      </body>
    </html>
  )
}
