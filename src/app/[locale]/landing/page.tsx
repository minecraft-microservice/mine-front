import { useTranslations } from 'next-intl'

export default function Index() {
  const t = useTranslations('pages')
  return (
      <h1>{t('landing.title')}</h1>
  )
}
