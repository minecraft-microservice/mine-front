import { ReactNode, useEffect } from 'react'

// Components
import MainNavigation from '@/components/navigation/MainNavigation'
import LandingFooter from '@/components/LandingFooter';

interface BasicLayoutProps {
  children: ReactNode;
}

export default function LandingLayout({ children }: BasicLayoutProps) {

  return (
    <div>
      <MainNavigation />
      {children}

      <LandingFooter />
    </div>
  )
}