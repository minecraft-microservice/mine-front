// Next
import { useTranslations } from 'next-intl'
import type { Metadata } from 'next'

// Components
import BasicLayout from '@/components/layout/BasicLayout'

export const metadata: Metadata = {
  title: 'About Page',
  description: 'About Page Description',
}

export default function Index() {
  const t = useTranslations('pages')
  return (
    <div>
      <h1>{t('about.title')}</h1>
    </div>
  )
}
