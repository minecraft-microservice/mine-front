import { useTranslations } from 'next-intl'

import type { Metadata } from 'next'

import BasicLayout from '@/components/layout/BasicLayout'

export const metadata: Metadata = {
  title: 'Free Minecraft Server Page',
  description: 'Free Minecraft Server Page Description',
}

export default () => {
  const t = useTranslations('pages')
  return (
    <div>
      <h1>{t('freeMinecraftServer.title')}</h1>
    </div>
  )
}
