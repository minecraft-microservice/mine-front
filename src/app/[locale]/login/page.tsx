'use client'

// Next
import { useTranslations } from 'next-intl'
import Image from 'next/image'

// Third-Parties
import { useForm, SubmitHandler } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import * as z from 'zod'

// Image
import { FaGithub, FaGoogle } from 'react-icons/fa'
import { MdEmail } from 'react-icons/md'
import { RiLockPasswordFill } from 'react-icons/ri'

type Inputs = {
  email: string
  password: string
}

const loginSchema = z.object({
  email: z.string().email().min(1, { message: 'Required' }),
  password: z.string().min(10),
})

type LoginSchema = z.infer<typeof loginSchema>

export default function Login() {
  const t = useTranslations('buttons')

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<LoginSchema>({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: zodResolver(loginSchema),
  })

  const onSubmit: SubmitHandler<Inputs> = (data) => console.log(data)

  const handleLoginClick: SubmitHandler<Inputs> = (data) => {
    console.log('data')
    console.log(data)
  }

  return (
    <div className="flex h-screen w-full items-center justify-center">
      <div
        className="fixed h-full w-full bg-cover bg-center blur-sm"
        style={{
          backgroundImage: 'url("/river.jpg")',
        }}
      />

      <div className="z-10 h-4/5 max-h-[500px] w-11/12 max-w-[800px] rounded-lg bg-slate-100 shadow-2xl shadow-slate-200 md:w-8/12">
        <div className="grid h-full md:grid-cols-2">
          <div
            className="hidden w-full rounded-s-lg bg-cover bg-center md:block"
            style={{
              backgroundImage: 'url("/pics/volcano.jpg")',
            }}
          />

          <div className="text-center">
            <div className="mx-4 mt-12">
              <label className="input input-bordered flex items-center gap-2">
                <MdEmail />
                <input
                  type="text"
                  className="grow"
                  placeholder="Email"
                  {...register('email')}
                />
              </label>

              {errors.email?.type === 'invalid_string' && (
                <p role="alert">Sring Value is Wrong</p>
              )}
            
              {errors.email?.type === 'Required' && (
                <p role="alert">Value is required</p>
              )}

              <label className="input input-bordered mt-2 flex items-center gap-2">
                <RiLockPasswordFill />
                <input
                  type="password"
                  className="grow"
                  placeholder="Password"
                  {...register('password')}
                />
              </label>

              <button
                className="btn btn-primary mt-6"
                onClick={handleSubmit(handleLoginClick)}
              >
                {t('login')}
              </button>

              <div className="mx-auto mt-5 grid max-w-72 grid-cols-2 gap-4">
                <button className="btn">
                  <FaGithub size="2em" />
                  Github
                </button>

                <button className="btn">
                  <FaGoogle size="2em" />
                  Gmail
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
