// hooks/useSocket.js
import { useEffect, useState } from 'react'
import io from 'socket.io-client'

const useSocket = (url) => {
  const [socket, setSocket] = useState(null)
  const [messages, setMessages] = useState([])

  useEffect(() => {
    const socketIo = io(url)

    setSocket(socketIo)

    socketIo.on('message', (message) => {
      setMessages((prevMessages) => [...prevMessages, message])
    })

    return () => {
      socketIo.disconnect()
    }
  }, [url])

  const sendMessage = (message) => {
    if (socket) {
      socket.emit('message', message)
    }
  }

  return { messages, sendMessage }
}

export default useSocket
