import { ReactNode, useEffect } from 'react'

interface BasicLayoutProps {
  children: ReactNode;
}

export default function BasicLayout({ children }: BasicLayoutProps) {

  return (
    <div>
      {children}
    </div>
  )
}