// pages/index.js
'use client'

import { useState } from 'react'
import useSocket from '../hooks/useSocket'

export default function Home() {
  const { messages, sendMessage } = useSocket('http://localhost:4000')
  const [input, setInput] = useState('')

  const handleSendMessage = () => {
    sendMessage(input)
    setInput('')
  }

  return (
    <div>
      <h1>Socket.io Chat</h1>
      <input
        type="text"
        value={input}
        onChange={(e) => setInput(e.target.value)}
        placeholder="Type a message"
      />
      <button onClick={handleSendMessage}>Send</button>
      <ul>
        {messages.map((message, index) => (
          <li key={index}>{message}</li>
        ))}
      </ul>
    </div>
  )
}
