import Link from 'next/link'
import { useTranslations, useLocale } from 'next-intl'

import { locales } from '@/utils/consts'

export default function LandingFooter() {
  const locale = useLocale()

  const t = useTranslations('navigation')

  return (
    <footer className="footer bg-neutral p-10 text-neutral-content">
      <nav>
        <h6 className="footer-title">Services</h6>
        <Link href={`/${locale}/landing/free-minecraft-server`}>
          {t('main.runFree')}
        </Link>
        <a className="link-hover link">Resource Packs</a>
        <a className="link-hover link">Shaders</a>
        <a className="link-hover link">Marketing</a>
        <a className="link-hover link">Advertisement</a>
      </nav>
      <nav>
        <h6 className="footer-title">Company</h6>
        <a className="link-hover link">About us</a>
        <a className="link-hover link">Contact</a>
      </nav>
      <nav>
        <h6 className="footer-title">Legal</h6>
        <a className="link-hover link">Terms of use</a>
        <a className="link-hover link">Privacy policy</a>
        <a className="link-hover link">Cookie policy</a>
      </nav>
    </footer>
  )
}
