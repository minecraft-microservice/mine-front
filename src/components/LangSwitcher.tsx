'use client'

import Link from 'next/link'
import { usePathname, useRouter } from 'next/navigation'
import { useTranslations, useLocale } from 'next-intl'

import { locales } from '@/utils/consts'

function getRestOfStringAfterSecondSlash(str: string) {
  const parts = str.split('/')
  return parts.slice(2).join('/')
}

export default function LangSwitcher() {
  const locale = useLocale()
  const router = useRouter()

  // console.log(`locale`)
  // console.log(locale)

  const pathname = usePathname()

  // console.log(`pathname`)
  // console.log(pathname)

  const handleOptionClick = (value: string) => {
    const url = `/${value.toLocaleLowerCase()}/${getRestOfStringAfterSecondSlash(
      pathname
    )}`

    router.push(url)
  }

  const t = useTranslations('navigation')
  return (
    <div className="dropdown dropdown-hover">
      <div tabIndex={0} role="button" className="btn m-1">
        {t('main.lang')}
      </div>
      <ul
        tabIndex={0}
        className="dropdown-content menu bg-base-100 rounded-box z-[1] w-52 p-2 shadow"
      >
        {locales.map((cur) => (
          <li key={cur} value={cur} onClick={() => handleOptionClick(cur)}>
            <a>{cur}</a>
            {/* <Link href={`/${cur}/dashboard`}>Dashboard</Link> */}
          </li>
        ))}
      </ul>
    </div>
  )
}
