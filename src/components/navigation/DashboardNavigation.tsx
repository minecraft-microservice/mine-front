import Link from 'next/link'
import { useTranslations, useLocale } from 'next-intl'

import LangSwitcher from '../LangSwitcher'

export default () => {
  const locale = useLocale()

  const themes = [
    'light',
    'dark',
    'cupcake',
    'bumblebee',
    'emerald',
    'corporate',
    'synthwave',
    'retro',
    'cyberpunk',
    'valentine',
    'halloween',
    'garden',
    'forest',
    'aqua',
    'lofi',
    'pastel',
    'fantasy',
    'wireframe',
    'black',
    'luxury',
    'dracula',
    'cmyk',
    'autumn',
    'business',
    'acid',
    'lemonade',
    'night',
    'coffee',
    'winter',
    'dim',
    'nord',
    'sunset',
  ]

  const t = useTranslations('navigation')

  const handleThemeModal = () => {
    document.getElementById('my_modal_2').showModal()
  }

  return (
    <div>
      <div className="navbar bg-base-100">
        <div className="flex-1">
          <a className="btn btn-ghost text-xl">{t('dashboard.title')}</a>
        </div>
        <div className="flex-none">
          <ul className="menu menu-horizontal px-1">
            <li>
              <a>Link</a>
            </li>
            <li>
              <details>
                {/* <summary>{t('dashboard.setting')}</summary> */}
                <summary>...</summary>
                <ul className="bg-base-100 rounded-t-none p-2">
                  <li>
                    <a>Link 1</a>
                  </li>
                  <li>
                    <a>Link 2</a>
                  </li>
                </ul>
              </details>
            </li>
            <li>
              <LangSwitcher />
            </li>
            <li>
              <a onClick={handleThemeModal}>Theme</a>
            </li>
          </ul>
        </div>
      </div>

      <dialog id="my_modal_2" className="modal">
        <div className="modal-box">
          <form method="dialog">
            {/* if there is a button in form, it will close the modal */}
            <button className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2">
              ✕
            </button>
          </form>
          <h3 className="font-bold text-lg">Title</h3>

          {themes.map((theme, index) => {
            return (
              <div data-theme={theme} key={index}>
                <button className="btn">A</button>
                <button className="btn btn-neutral">A</button>
                <button className="btn btn-primary">A</button>
                <button className="btn btn-secondary">A</button>
                <button className="btn btn-accent">A</button>
                <button className="btn btn-ghost">A</button>
                <button className="btn btn-link">A</button>
              </div>
            )
          })}


          <div className="modal-action">
            <form method="dialog">
              {/* if there is a button, it will close the modal */}
              <button className="btn">Close</button>
            </form>
          </div>
        </div>
        <form method="dialog" className="modal-backdrop">
          <button>close</button>
        </form>
      </dialog>
    </div>
  )
}
