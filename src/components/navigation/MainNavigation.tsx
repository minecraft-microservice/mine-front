import Link from 'next/link'
import { useTranslations, useLocale } from 'next-intl'

import LangSwitcher from '../LangSwitcher'

export default () => {
  const locale = useLocale()

  const t = useTranslations('navigation')
  return (
    <div>
      <div>
        <button className="btn btn-outline btn-primary">
          <Link href={`/${locale}/landing`}>{t('main.home')}</Link>
        </button>
        <button className="btn btn-outline btn-primary">
          <Link href={`/${locale}/landing/free-minecraft-server`}>
            {t('main.runFree')}
          </Link>
        </button>
        <button className="btn btn-outline btn-primary">
          <Link href={`/${locale}/landing/about`}>{t('main.about')}</Link>
        </button>
        <button className="btn btn-outline btn-primary">
          <Link href={`/${locale}/dashboard/login`}>{t('main.dashboard')}</Link>
        </button>
      </div>

      <LangSwitcher />
    </div>
  )
}
