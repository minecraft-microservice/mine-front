export default function Text() {

return (
  <label className="form-control w-full max-w-xs">
          <div className="label">
            <span className="label-text">Name</span>
            <span className="label-text-alt">Required</span>
          </div>
          <input
            type="text"
            placeholder="Type here"
            className="input input-bordered w-full max-w-xs"
          />
          <div className="label">
            <span className="label-text-alt">Bottom Left label</span>
            {/* <span className="label-text-alt">Bottom Right label</span> */}
          </div>
        </label>
)

}