interface MyComponentProps {
  title: string
  isActive: boolean
  options: (string | OptionObject)[]
  onClick: () => void
}

function isArrayOfStrings(
  options: (string | OptionObject)[]
): options is string[] {
  return options.every((option) => typeof option === 'string')
}

function isOptionObject(option: string | OptionObject): option is OptionObject {
  return (
    (option as OptionObject).value !== undefined &&
    (option as OptionObject).label !== undefined
  )
}

function isArrayOfOptionObjects(
  options: (string | OptionObject)[]
): options is OptionObject[] {
  return options.every(isOptionObject)
}

export default function Select({
  title,
  isActive = true,
  options,
}: MyComponentProps) {
  const optionItems = []

  if (isArrayOfStrings(options))
    optionItems.push(
      ...options.map((option, index) => (
        <option key={index} disabled={false}>
          {option}
        </option>
      ))
    )
  else if (isArrayOfOptionObjects(options))
    optionItems.push(
      ...options.map((option, index) => (
        <option key={index} disabled={option.disabled}>
          {option.label}
        </option>
      ))
    )

  // export default function Select() {
  return (
    <label className="form-control w-full max-w-xs">
      <div className="label">
        <span className="label-text">{title}</span>
        <span className="label-text-alt">Required</span>
      </div>
      <select className="select select-bordered" defaultValue={'Harry Potter'}>
        {optionItems}
      </select>

      <div className="label">
        <span className="label-text-alt">Alt label</span>
        {/* <span className="label-text-alt">Alt label</span> */}
      </div>
    </label>
  )
}
